# Movie List (Client Side)

Movie App Built with React, Apollo and GraphQL

### Spec
>- React.js
>- Apollo Framework for GraphQL
>- GraphQL

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn start
```

### Compiles and minifies for production
```
yarn build
```

### Compiles and test
```
yarn test
```